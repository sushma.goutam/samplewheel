# SampleWheel
Sample app for generating python wheel 

The CI/CD pipeline is configured in GitLab.
Whenever a new commit is made to this GitHub repository, a build is triggered in GitLab build server and new package wheel is created.
The generated build arifact (python wheel) can be downloaded from [here](https://gitlab.com/sushma.goutam/samplewheel/-/pipelines).
