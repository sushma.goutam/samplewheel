from setuptools import setup

setup(
    name='sample_wheel',
    version='2.0.0',
    description='Sample application for generating Python wheel',
    url='https://www.google.com',
    author='Sushma Goutam',
    author_email='sushma.goutam@perkinelmer.com',
    license="MIT",
    platforms=['windows', 'linux'],
    python_requires='>=3.7.3'
)
